# simple static heroku example

  - Just one little line in `Procfile`, using python or ruby, your choice.
  - Put your html/css/js/images in `public/`.
  - Create your "app" with a null buildpack:

```
heroku create --stack cedar --buildpack http://github.com/ryandotsmith/null-buildpack.git
```
